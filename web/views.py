# -*- coding: utf-8 -*-
from django.http import JsonResponse
from django.shortcuts import render
from django.views.generic import TemplateView
from rest_framework import viewsets
from rest_framework.decorators import api_view

from web.models import Pet
from web.serializers import PetSerializer


class IndexPageView(TemplateView):
    def get(self, request, **kwargs):
        return render(request, 'index.html', context=None)


class PetView(viewsets.ModelViewSet):

    queryset = Pet.objects.all()
    serializer_class = PetSerializer


@api_view(['GET', ])
def voto(request, pet):
    p = Pet.objects.get(id=pet)

    return JsonResponse({"votos": p.vote()}, safe=False)
