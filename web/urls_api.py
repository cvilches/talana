from django.conf.urls import url

from web import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'pets', views.PetView)

urlpatterns = [
    url(r'^(?P<pet>[0-9])/vote/$', views.voto),
]

urlpatterns += router.urls
