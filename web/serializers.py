from rest_framework import serializers
from web.models import Pet, Vote


class PetSerializer(serializers.ModelSerializer):
    # job_type = serializers.CharField(source='task_type')

    class Meta:
        model = Pet
        fields = '__all__'

class VoteSerializer(serializers.ModelSerializer):
    # job_type = serializers.CharField(source='task_type')

    class Meta:
        model = Vote
        fields = '__all__'