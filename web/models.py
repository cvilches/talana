# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Pet(models.Model):
    nickname = models.CharField(max_length=25)
    pet_name = models.CharField(max_length=50)
    photo_pet = models.FileField(upload_to='pets/', blank=True, null=True)
    votes = models.IntegerField(default=0)

    def vote(self):
        self.votes += 1
        self.save()
        return self.votes

    def __unicode__(self):
        return self.pet_name + self.nickname


class Vote(models.Model):
    photo_pet = models.ForeignKey(Pet, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    ip_host = models.GenericIPAddressField()

    def __unicode__(self):
        return self.ip
